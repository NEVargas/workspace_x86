/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

#define REPOSO      1   // nombres para estados 
#define ESPERANDO   2
#define INGRESANDO  3
#define ALARMA      4

#define TIEMPO_ESPERA 5 // tiempo de espera cuando pasa la barrera

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

char state;
char evSensor1_on;
char evSensor2_on;
char evSensor2_off;
int evTime1seg;
int count_seg;


/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);

    state = REPOSO;
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}




/* ======================[ indican acciones a realizar]=======================*/
void hw_AbrirBarrera(void)
{
    printf("Barrera abierta \n\n");
}

void hw_CerrarBarrera(void)
{
    printf("Barrera cerrada \n\n");
}

void hw_encender_Buzzer(void)
{
    printf("Alarma activada \n\n");
}

void hw_apagar_Buzzer(void)
{
    printf("Alarma desactivada \n\n");
}


/*=================================[ eventos ]===============================*/
void hw_raised_Sensor1_on(void)
{
    evSensor1_on = 1;     
}

void hw_raised_Sensor2_on(void)
{
    evSensor2_on = 1;
    
}

void hw_raised_Sensor2_off(void)
{
    evSensor2_off = 1;        
}

void hw_raised_evTime1seg(void)
{
    evTime1seg = 1;
}


/*=========================[ reseteo de eventos]=============================*/
void clear_estados(void)
{
    evSensor1_on    =   0;
    evSensor2_off   =   0;
    evTime1seg      =   0;
    evSensor2_on    =   0;       

}


/*========================[ indica estado actual ]===========================*/
void estado_actual(void)
{
    if(state == REPOSO){
        printf("Estado actual:  REPOSO \n\n");
    }
    else if(state == INGRESANDO){
        printf("Estado actual:  INGRESANDO \n\n");
    }
    else if(state == ESPERANDO){
        printf("Estado actual:  ESPERANDO \n\n");
    }
    else if(state == ALARMA){
        printf("Estado actual:  ALARMA \n\n");
    }
}


/*==================[ superloop maquina de estados]==========================*/
void garaje(void)
{
    switch(state){
        case REPOSO:
            if (evSensor1_on){
                hw_AbrirBarrera();
                state = INGRESANDO;
            }
            else if (evSensor2_on){
                hw_encender_Buzzer();
                state = ALARMA;
            }
        break;

        case INGRESANDO:
            if (evSensor2_on){
                count_seg = 0;
                state = ESPERANDO;
            }
        break;

        case ESPERANDO:
            if (evTime1seg && (count_seg < TIEMPO_ESPERA) ){
                count_seg ++;
                state = ESPERANDO;
            }
            else if (evTime1seg && (count_seg == TIEMPO_ESPERA)){
                hw_CerrarBarrera();
                state = REPOSO;
            }
            else if (evSensor1_on){
                state = INGRESANDO;
            }

        break;

        case ALARMA:
            if(evSensor2_off){
                hw_apagar_Buzzer();
                state = REPOSO;
            }
        break;
    }

    clear_estados();
}



/*==================[end of file]============================================*/
