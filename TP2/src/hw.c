/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

#define REPOSO                  0
#define ESPERANDO_SELECCION     1
#define ESPERANDO_CAFE          2
#define ESPERANDO_TE            3
#define PREPARADO               4

#define TIEMPO_ESPERA_SEG_CAFE      45
#define TIEMPO_ESPERA_SEG_TE        30
#define TIEMPO_ESPERA_SEG_SELECCION 30  
#define TIEMPO_ESPERA_SALIDA        2              

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

char state;
char evIngresa_moneda;
char evTime_1seg;
char evTime_100ms;
char evTime_500ms;
char evBoton_cafe;
char evBoton_te;
int cont_seg;

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

/*====================[ superloop de maquina de estados ]======================*/
void expendedora(void)
{
    switch(state){
        case REPOSO:
            if (evIngresa_moneda){
                cont_seg = 0;
                state = ESPERANDO_SELECCION;
            }
            else if (evTime_500ms){
                hw_toggle1Hz_led();
                state = REPOSO;
            }
        break;
        
        case ESPERANDO_SELECCION:
            if (evBoton_cafe){
                hw_cafe_on();
                state = ESPERANDO_CAFE;
            }
            else if (evBoton_te){
                hw_te_on();
                state = ESPERANDO_TE;
            }

            else if (evTime_1seg && (cont_seg < TIEMPO_ESPERA_SEG_SELECCION)){
                cont_seg ++;
                state = ESPERANDO_SELECCION;
            }
            else if (evTime_1seg && (cont_seg == TIEMPO_ESPERA_SEG_SELECCION)){
                hw_devolver_moneda();
                state = REPOSO;
            }

            else if (evTime_500ms){
                hw_toggle1Hz_led();
                state = ESPERANDO_SELECCION;
            }
        break;

        case ESPERANDO_CAFE:
            if (evTime_1seg && (cont_seg < TIEMPO_ESPERA_SEG_CAFE)){
                cont_seg ++;
                state = ESPERANDO_CAFE;
            }
            else if (evTime_1seg && (cont_seg == TIEMPO_ESPERA_SEG_CAFE)){
                hw_cafe_off();
                hw_alarma_on();
                cont_seg = 0;
                state = PREPARADO;
            }

            else if (evTime_100ms){
                hw_toggle5Hz_led();
                state = ESPERANDO_CAFE;
            }

        break;

        case ESPERANDO_TE:
            if (evTime_1seg && (cont_seg < TIEMPO_ESPERA_SEG_TE)){
                cont_seg ++;
                state = ESPERANDO_TE;
            }
            else if (evTime_1seg && (cont_seg == TIEMPO_ESPERA_SEG_TE)){
                hw_te_off();
                hw_alarma_on();
                cont_seg = 0;
                state = PREPARADO;
            }

            else if (evTime_100ms){
                hw_toggle5Hz_led();
                state = ESPERANDO_TE;
            } 
        break;

        case PREPARADO:
            if (evTime_1seg && (cont_seg < TIEMPO_ESPERA_SALIDA)){
                cont_seg ++;
                state = PREPARADO;
            }
            else if (evTime_1seg && (cont_seg == TIEMPO_ESPERA_SALIDA)){
                hw_alarma_off();
                state = REPOSO;
            }
            
            else if (evTime_500ms){
                hw_toggle1Hz_led();
                state = PREPARADO;
            }

        break;
    }

    clear_estados();
}

/*============================[ resetea eventos ]============================*/
void clear_estados(void)
{
    evBoton_cafe        =   0;
    evBoton_te          =   0;
    evIngresa_moneda    =   0;
    evTime_1seg         =   0;
    evTime_100ms        =   0;  
    evTime_500ms        =   0;   
}

/*========================[ indica el estado actual ]==========================*/
void hw_estado_actual(void)
{
    if(state == REPOSO){
        printf("Estado actual   :   REPOSO \n\n");
    }
    else if (state == ESPERANDO_SELECCION)
    {
        printf("Estado actual   :   ESPERANDO SELCCION \n\n");
    }
     else if (state == ESPERANDO_CAFE)
    {
        printf("Estado actual   :   ESPERANDO CAFE \n\n");
    }
    else if (state == ESPERANDO_TE)
    {
        printf("Estado actual   :   ESPERANDO TE \n\n");
    }
    else if (state == PREPARADO)
    {
        printf("Estado actual   :   PREPARADO \n\n");
    }   
}

/*=======================[ indica accion a realizar ]==========================*/
void hw_alarma_on(void)
{
    printf("Alarma activada \n\n");
}
void hw_alarma_off(void)
{
    printf("Alarma desactivada \n\n");
}



void hw_te_on(void)
{
    printf("Preparando cafe \n\n");
}
void hw_cafe_on(void)
{
    printf("Preparando té \n\n");
}


void hw_te_off(void)
{
    printf("Té listo \n\n");
}
void hw_cafe_off(void)
{
    printf("Cafe listo \n\n");
}


void hw_devolver_moneda(void)
{
    printf("Se devuelve moneda por pasar el tiempo de espera \n\n");
}

void hw_toggle1Hz_led(void)
{
    printf("Parpadea led de 1Hz \n\n");
}
void hw_toggle5Hz_led(void)
{
    printf("Parpadea led de 5Hz \n\n");
}

/*==================================[ eventos ]=================================*/

void hw_raised_Boton_cafe(void)
{
    evBoton_cafe = 1;
}
void hw_raised_Boton_te(void)
{
    evBoton_te = 1;
}
void hw_raised_Ingresa_moneda(void)
{
    evIngresa_moneda = 1;
}

void hw_raised_Time_1seg(void)
{
    evTime_1seg = 1;
}
void hw_raised_Time_500ms(void)
{
    evTime_500ms = 1;
}
void hw_raised_Time_100ms(void)
{
    evTime_100ms = 1;
}

/*==================[end of file]============================================*/
