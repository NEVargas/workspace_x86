/*==================[inclusions]=============================================*/

#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/
#define n 5
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void) 
{
    printf("¡Hola mundo!\n\n");
    return 0;
}
/*==================[end of file]============================================*/
