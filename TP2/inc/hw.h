#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT        27  // ASCII para la tecla Esc 
#define SENSOR_1    49  // ASCII para la tecla 1 
#define SENSOR_2    50  // ASCII para la tecla 2 
#define SENSOR_3    51  // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void expendedora(void);

void hw_alarma_on(void);
void hw_alarma_off(void);
void hw_cafe_on(void);
void hw_cafe_off(void);
void hw_te_on(void);
void hw_te_off(void);
void hw_devolver_moneda(void);
void hw_toggle1Hz_led(void);
void hw_toggle5Hz_led(void);


void hw_raised_Boton_cafe(void);
void hw_raised_Boton_te(void);
void hw_raised_Ingresa_moneda(void);
void hw_raised_Time_1seg(void);
void hw_raised_Time_100ms(void);
void hw_raised_Time_500ms(void);

void clear_estados(void);
void hw_estado_actual(void);


/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
