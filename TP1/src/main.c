/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int cont_ms = 0;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            hw_raised_Sensor1_on();
        }

        if (input == SENSOR_2) {
            hw_raised_Sensor2_on();
        }
        else{
            hw_raised_Sensor2_off();
        }

        cont_ms ++;
        if (cont_ms == 1000){ 
            cont_ms = 0;
            hw_raised_evTime1seg();
            estado_actual();
        }

        garaje();

        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
