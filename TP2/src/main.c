/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

#define TIEMPO_ESPERA_MS_LED1HZ    500
#define TIEMPO_ESPERA_MS_LED5HZ    100

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int cont_ms     =   0;
    int cont2_ms    =   0;
    int cont3_ms    =   0;

    hw_Init();

    
    while (input != EXIT) {                 // Superloop hasta que se presione la tecla Esc
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            hw_raised_Ingresa_moneda();
        }

        if (input == SENSOR_2) {
            hw_raised_Boton_cafe();
        }

        if (input == SENSOR_3){
            hw_raised_Boton_te();
        }

        cont_ms  ++;    
        cont2_ms ++;
        cont3_ms ++;


        if (cont_ms == 1000){ 
            cont_ms = 0;
            hw_raised_Time_1seg();
            hw_estado_actual();
        }

        if(cont2_ms == TIEMPO_ESPERA_MS_LED1HZ){
            cont2_ms = 0;
            hw_raised_Time_500ms();
        }

        if(cont3_ms == TIEMPO_ESPERA_MS_LED5HZ){
            cont3_ms = 0;
            hw_raised_Time_100ms();
        }

        expendedora();       

        hw_Pausems(1);
    }

 

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
