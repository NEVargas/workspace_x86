/**
 * @author Nahuel Vargas
 */

#ifndef _HW_H_
#define _HW_H_

/** \addtogroup TP1
 ** @{ */

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT        27  // ASCII para la tecla Esc 
#define SENSOR_1    49  // ASCII para la tecla 1 
#define SENSOR_2    50  // ASCII para la tecla 2   

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


/**
 * @brief Inicializa la maquina de estado
 */
void hw_Init(void);

void hw_DeInit(void);
void hw_Pausems(uint16_t t);

/**
 * @brief Lee la entrada del teclado
 * @return codigo ASCII de tecla presionada
 */
uint8_t hw_LeerEntrada(void);

/** @name Mensajes
 * Muestran mensajes en la consola
 */
///@{
void hw_AbrirBarrera(void);
void hw_CerrarBarrera(void);
void hw_encender_Buzzer(void);
void hw_apagar_Buzzer(void);
///@}

/**
 * @name Raised
 * Indican que los sensores estan activados/desactivados
 */
///@{
void hw_raised_Sensor1_on(void);
void hw_raised_Sensor2_on(void);
void hw_raised_Sensor2_off(void);
void hw_raised_evTime1seg(void);
/**
 * @brief Vuelven a estado inicial
 */
void clear_estados(void);
///@}

/**
 * @brief Indica estado actual en consola
 */
void estado_actual(void);

/**
 * @brief Maquina de estados
 */
void garaje(void);


/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
